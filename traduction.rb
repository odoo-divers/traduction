#!/usr/bin/env ruby
# coding: utf-8

require 'bundler/setup'
require 'ooor'
require 'yaml'

config = YAML.load_file("configuration.yaml")
Ooor.logger.level = config["Odoo"]["log_level"]
Ooor.new({:url => config["Odoo"]["url"], :database => config["Odoo"]["database"], :username => config["Odoo"]["username"], :password => config["Odoo"]["password"]})

mots = YAML.load_file("mots.yaml")

mots.each do |mot,traduction|
	lignes = IrTranslation.where(:lang => "fr_FR", :src => mot, :state => "to_translate")
	lignes.each do |ligne|
		ligne.value = traduction
		ligne.save
		puts "Ligne #{ligne.id} : traduction du mot #{mot} en #{traduction}"
	end
	puts "Mot #{mot} déjà traduit" if lignes.count==0
end
